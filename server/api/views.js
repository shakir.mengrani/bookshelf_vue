const models = require("./models");
const book = new models("Book");
const page = new models("Page");
module.exports = {
    addBook: (req, res) => {
        book.insert({name: req.body.name}, (err, doc) => {
            if(err){
                res.status(500).json({"error": err})
            }else{
                res.status(200).json(doc);
            }
        });
    },
    
    addPage: (req, res) => {
        page.insert({book: req.body.book, page_number: req.body.page_number, content: req.body.content}, (err, doc) => {
            if(err){
                res.status(500).json({"error": err});
            }else{
                doc.once("es-indexed", (err, resp) => {
                    if(err){
                        doc.remove()
                        res.status(500).json({"error": err});
                    }else{
                        doc.search_id = resp._id;
                        doc.save();
                        res.status(200).json(resp);
                    }
                })
            }
        });
    },

    getBook: (req, res) => {
        book.get((err, docs)=>{
            if(err){
                res.status(500).json({"error": err});
            }else{
                res.status(200).json(docs);
            }
        });
    },

    getPage: (req, res) => {
        page.getCollection()
        .find({book: req.params.book})
        .populate({path: "book", model: book.getCollection(), select: ["name"]})
        .sort("page_number")
        .exec((err, docs) => {
            if(err){
                res.status(500).json({"error": err})
            }else{
                let data = docs.map((doc) => {
                    return {
                        "_id": doc._id,
                        "book": doc.book.name,
                        "page_number": doc.page_number,
                        "content": doc.content    
                    }
                })
                res.status(200).json(data);
            }
        });
    },

    getPageById: (req, res) => {
        page.getCollection()
        .findOne({book: req.params.book, _id: req.params.page})
        .populate({path: "book", model: book.getCollection(), select: ["name"]})
        .exec((err, doc) => {
            if(err){
                res.status(500).json({"error": err})
            }else{
                res.status(200).json({
                    "_id": doc._id,
                    "book": doc.book.name,
                    "page_number": doc.page_number,
                    "content": doc.content
                });
            }
        });
    },

    search: (req, res) => {
        page.getCollection()
        .search({
            query_string:{
                query: req.query.q
            }
        }, (err, result) => {
            if(err){
                res.status(500).json({"error": err})
            }else{
                let docs = result.hits.hits.map((doc) => {
                    return doc._id;
                });
                page.getCollection()
                .find({"search_id": {$in: docs}})
                .populate({path:"book", model: book.getCollection(), select:["name"]})
                .exec((err, docs) => {
                    if(err){
                        res.status(500).json({"error": err});
                    }else{
                        let data = docs.map((doc) => {
                            return {
                                "book": doc.book.name,
                                "page_number": doc.page_number,
                                "content": doc.content
                            }
                        });
                        res.status(200).json(data);
                    }
                });
            }
        })
    }
};