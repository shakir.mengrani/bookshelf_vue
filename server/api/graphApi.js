const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const models = require("./models");
const { typeDefs } = require("./schema")
const book = new models("Book");
const page = new models("Page");
const app = express();

const resolvers = {
    Query:{
        hello: () => { return "Hello World"; },
        book: () => {
            return book.getCollection().find({}).exec()
            .then((docs) => {
                return docs
            })
        },
        page: (parent, args, context, info) => {
            return page.getCollection()
            .find({book: args.id})
            .populate({path: "book", model: book.getCollection(), select: ["name"]})
            .sort("page_number")
            .exec().then((docs) => {
                let data = docs.map((doc) => {
                    return {
                        "_id": doc._id,
                        "book": doc.book,
                        "page_number": doc.page_number,
                        "content": doc.content    
                    }
                })

                return data;
            })
        }
    }
};
const graphQLServer = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({req}) => {
        const token = req.headers.authorization || '';
        // return {token}; 
    },
    playground: true,
});
graphQLServer.applyMiddleware({app: app, path: "/graph"});
module.exports = app;