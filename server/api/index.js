const express = require("express");
const bodyParser = require("body-parser");
const views = require("./views")
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.status(200).json({"tagline": "Create own book shelf"})
});

app.get("/book", views.getBook);
app.post("/book", views.addBook);

app.get("/book/:book", views.getPage);
app.get("/book/:book/page/:page", views.getPageById);
app.post("/page", views.addPage);

app.get("/search", views.search);

module.exports = app;