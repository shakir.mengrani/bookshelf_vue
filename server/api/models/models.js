const mongoose = require("mongoose");
const mongoosastic = require('mongoosastic')
const { ObjectId } = mongoose.Schema.Types;
// Documents schema
module.exports = {
    Author: new mongoose.Schema({
        name: {type: String, required: true}
    }),

    Publisher: new mongoose.Schema({
        name: {type: String, required: true}
    }),

    Book: new mongoose.Schema({
        name: {type: String, required: true},
        author: {type: ObjectId},
        publisher: {type: ObjectId},
        isban: {type: String, required: false, default: 'None'}
    }),

    Page: new mongoose.Schema({
        book: {type: ObjectId, required: true, es_indexed: true},
        page_number: {type: Number, required: true, es_indexed: true},
        content: {type: String, required: true, es_indexed: true},
        search_id: {type: String}
    }).plugin(mongoosastic,{
        hosts:['localhost:9200'],
        populate:{path: "book"}
    }),
    
}