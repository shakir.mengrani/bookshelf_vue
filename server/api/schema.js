const { gql } = require("apollo-server");

exports.typeDefs = gql`
type Book {
    _id: String,
    name: String
}
type Page {
    _id: String,
    book: Book,
    page_number: Int,
    content: String
}
type Query {
    hello: String,
    book: [Book],
    page(id:String!): [Page]
}
`