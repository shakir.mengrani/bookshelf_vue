const express = require('express')
const consola = require('consola')
const mongoose = require("mongoose");
const api = require("./api")
const graphApi = require("./api/graphApi")
const { Nuxt, Builder } = require('nuxt')
const app = express()

// setup database
mongoose.connect("mongodb://localhost:27017/bookshelf", { useNewUrlParser: true }).catch(err => Consola.error(err))

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }
  // setup graphql route
  app.use(graphApi)
  // setup api route
  app.use("/api", api);
  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
